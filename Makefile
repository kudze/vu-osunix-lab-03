CC=gcc
CFLAGS=-Wall -g

all: myClient myServer

myClient: client/main.c
	$(CC) -o bin/client client/main.c $(CFLAGS)

myServer: server/main.c
	$(CC) -o bin/server server/main.c $(CFLAGS)
