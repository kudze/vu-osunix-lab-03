#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <dirent.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#define TRUE 1
#define IS_SERVER

#include "../shared/shared.h"
#define MAX_CLIENTS 100

typedef struct {
    bool used;
    SOCKET socket;
    struct sockaddr_in address;
} Client;

Client clientBuffer[MAX_CLIENTS];
fd_set fileDescriptors;

void initializeClientBuffer() {
    for (unsigned int i = 0; i < MAX_CLIENTS; i++) {
        clientBuffer[i].used = false;
    }
}

/**
 * Funkcija, kuri paruošia failu descriptorius select funkcijai.
 * Gražina maxfileDescriptor kuris naudojamas select'e.
 *
 * @param server_socket
 * @return
 */
int prepareFileDescriptorSetForSelect(SOCKET server_socket) {
    int maxfd = server_socket;

    FD_ZERO(&fileDescriptors);
    FD_SET(server_socket, &fileDescriptors);

    for (int i = 0; i < MAX_CLIENTS; i++) {
        if (clientBuffer[i].used) {
            FD_SET(clientBuffer[i].socket, &fileDescriptors);

            if (clientBuffer[i].socket > maxfd)
                maxfd = clientBuffer[i].socket;
        }
    }

    return maxfd;
}

/**
 * Padaro, kad socketa butu galima užbindint ant to pačio adreso jei prieš tai užbindintas .
 *
 * @param socket
 * @return
 */
bool configureSocketForReusingAddress(SOCKET socket)
{
    int opt = TRUE;
    int optres = setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, (const char *) &opt, sizeof(opt));

    if (optres < 0) {
        close(socket);

        return false;
    }

    return true;
}

/**
 * Funkcija skirta prijungti socketą prie serverio.
 *
 * @param socket
 * @param address
 * @return bool - true on success.
 */
bool bindSocketToAddress(SOCKET socket, struct sockaddr_in const *address) {
    write_log("Binding %u socket to:", socket);
    printAddress(address);

    int result = bind(socket, (const struct sockaddr *) address, sizeof(struct sockaddr_in));
    if (result == INVALID_SOCKET) {
        write_error();

        return false;
    }

    write_log("Successfully binded!");

    return true;
}

/**
 * Funkcija skirta padaryti socketą klausytis prisijungimų.
 *
 * @param socket
 * @param maxPendingConnections
 * @return
 */
bool makeSocketListenToConnections(SOCKET socket, int maxPendingConnections) {
    int res = listen(socket, maxPendingConnections);

    if (res < 0) {
        write_error();

        return false;
    }

    write_log("Socket %u is listening for new connections!", socket);

    return true;
}

/**
 * Finds first unused client id.
 * Otherwise returns -1
 *
 * @return clientID
 */
int findUnusedClientID() {
    for (int i = 0; i < MAX_CLIENTS; i++) {
        if (!clientBuffer[i].used)
            return i;
    }

    return -1;
}

/**
 * Funkcija skirta socket'ui priimti prisijungimą.
 *
 * @param socket
 * @return
 */
int makeSocketAcceptConnection(SOCKET socket) {
    int clientID = findUnusedClientID();

    if (clientID == -1)
        return clientID;

    socklen_t clientAddressSize = sizeof(clientBuffer[clientID].address);
    clientBuffer[clientID].socket = accept(socket, (struct sockaddr *) &(clientBuffer[clientID].address),
                                           &clientAddressSize);

    if (clientBuffer[clientID].socket == INVALID_SOCKET) {
        write_error();
    }

    clientBuffer[clientID].used = true;

    write_log("Client (%d) connected!", clientID);
    printAddress(&(clientBuffer[clientID].address));

    return clientID;
}

/**
 * Funckija skirta nuskaityti direktorijos failus.
 *
 * @param directory
 * @param buffer
 * @param n
 * @return
 */
bool getFilesInDirectoryAsString(const char *directory, char *buffer, unsigned int n) {

    DIR* dfd = opendir(directory);
    if(dfd == NULL)
        return false;

    struct dirent* dp;
    while((dp = readdir(dfd)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0)
            continue;

        if(strcmp(dp->d_name, "..") == 0)
            continue;

        size_t dlen = strlen(dp->d_name);
        size_t blen = strlen(buffer);

        if (blen == 0)
            strncpy(buffer, dp->d_name, n);

        else
            strncat(buffer, dp->d_name, n);

        n -= dlen;
        strncat(buffer, "\n", n--);
    }

    return true;
}

/**
 * You need to free(result[i]), where result := return value, i > 0, i <= (*filesCount)
 * @return
 */
char** getFilesInDirectoryAsArray(const char *directory, unsigned long* filesCount) {

    DIR* dfd = opendir(directory);
    if(dfd == NULL)
        return false;

    unsigned long mainArraySize = 10; //lets start at 10.
    char** mainArray = malloc(sizeof(char*) * mainArraySize);

    *filesCount = 0;
    struct dirent* dp;
    while((dp = readdir(dfd)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0)
            continue;

        if(strcmp(dp->d_name, "..") == 0)
            continue;

        unsigned long filename_len = strlen(dp->d_name);
        char* filenameEntry = calloc(filename_len + 1, sizeof(char));
        strncpy(filenameEntry, dp->d_name, filename_len);

        if((*filesCount) == mainArraySize) {
            mainArraySize += mainArraySize;
            mainArray = realloc(mainArray, mainArraySize);

            if(mainArray == NULL)
                return mainArray;
        }

        mainArray[(*filesCount)++] = filenameEntry;
    }

    return mainArray;
}

/**
 * Funckija skirta suhandlinti kai klientas atsijungia.
 *
 * @param clientID
 */
void handleDisconnect(int clientID) {
    if(!clientBuffer[clientID].used)
        return;

    close(clientBuffer[clientID].socket);

    clientBuffer[clientID].used = false;
    clientBuffer[clientID].socket = -1;
    memset(&(clientBuffer[clientID].address), 0, sizeof(clientBuffer[clientID].address));

    write_log("Client (%d) disconnected!", clientID);
}

/**
 * Funkcija skirta suhandlinti kliento atsiusta info.
 *
 * @param clientID
 * @param message
 */
void handleClientMessage(int clientID, const char *message, SOCKET server_socket) {
    SOCKET client_socket = clientBuffer[clientID].socket;
    write_log("Recieved message from client %d (socket %d)", clientID, client_socket);

    if (strcmp(message, "list") == 0) {
        write_log("Client wanted to list all available files!");

        char buffer[1024];
        memset(buffer, 0, sizeof(buffer));
        getFilesInDirectoryAsString("data", buffer, sizeof(buffer));

        if(strlen(buffer) == 0)
        {
            strcpy(buffer, "Server directory is empty!");
        }

        send(client_socket, buffer, (int) strlen(buffer), 0);
    } else if (strncmp(message, "get|", 4) == 0) {
        char filename[128];
        strcpy(filename, message + 4);

        write_log("Client wanted to download %s file!", filename);

        if(!checkIfSafePath(filename))
        {
            write_log("Path started with . or /, this is not allowed due to security reasons.");
            clearSocketBuffer(client_socket);

            writeNoFileFoundIntoSocket(client_socket);
            return;
        }

        char fullFilename[256];
        snprintf(fullFilename, 256, "data/%s", filename);

        FILE *file = fopen(fullFilename, "rb");
        if (file == NULL) {
            write_log("File was not found!");
            clearSocketBuffer(client_socket);

            writeNoFileFoundIntoSocket(client_socket);
            return;
        }

        bool result = writeFileIntoSocket(client_socket, file);
        fclose(file);

        if(result)
            write_log("File was successfully fetched!");

        else
            write_log("Client could not fetch file :(");
    } else if (strncmp(message, "upload|", 7) == 0) {
        char filename[128];
        memset(filename, 0, sizeof(filename));
        strcpy(filename, message + 7);

        if(!checkIfSafePath(filename))
        {
            write_error("Client tried to hack us :(");
            clearSocketBuffer(client_socket);

            writeNoFileFoundIntoSocket(client_socket);
            return;
        }

        char fullFilename[256];
        snprintf(fullFilename, 256, "data/%s", filename);
        FILE* file = fopen(fullFilename, "wb");
        if(file == NULL)
        {
            write_error("Could not create the %s file!", fullFilename);
            clearSocketBuffer(client_socket);

            writeNoFileFoundIntoSocket(client_socket);
            return;
        }

        write_log("Reading \"%s\" file from %d socket (%d client) to \"%s\" file!", filename, client_socket, clientID, fullFilename);
        bool valid = readFileFromSocket(client_socket, file);
        if(!valid) {
            fclose(file);
            remove(fullFilename);

            write_log("Reading file from socket %d went unsuccessfully as some data was lost!", client_socket);
            write_log("Clearing out socket buffer...");
            clearSocketBuffer(client_socket);

            write_log("Reporting to client...");
            writeNoFileFoundIntoSocket(client_socket);
            return;
        }

        fclose(file);
        write_log("%s uploaded!", filename);

        write_log("Reporting to client...");
        int write = snprintf(buffer, BUFFER_LENGTH, "OK");
        send(client_socket, buffer, write, 0);
    } else if(strcmp(message, "clone") == 0) {
        write_log("Client wants to clone the server");
        shutdown(client_socket, SHUT_RD);

        unsigned long files_count, files_count_net_order;
        char** filenames = getFilesInDirectoryAsArray("data", &files_count);
        files_count_net_order = htobe64(files_count);
        send(client_socket, &files_count_net_order, sizeof(files_count_net_order), 0);

        for(unsigned long i = 0; i < files_count; i++) {
            char* filename = filenames[i];
            write_log("Sending %s to client!", filename);

            unsigned long filename_len = strlen(filename), filename_len_net_order;
            filename_len_net_order = htobe64(filename_len);
            send(client_socket, &filename_len_net_order, sizeof(filename_len_net_order), 0);
            send(client_socket, filename, filename_len, 0);

            char fullFilename[256];
            snprintf(fullFilename, 256, "data/%s", filename);
            FILE *file = fopen(fullFilename, "rb");
            if(file == NULL) {
                write_named_error("Could not open file whilst writing clone data");

                for(unsigned long j = i; j < files_count; j++)
                    free(filenames[j]);
                break;
            }

            bool valid = writeFileIntoSocket(client_socket, file);
            if(!valid) {
                fclose(file);
                write_log("Failed to write file to socket!");

                for(unsigned long j = i; j < files_count; j++)
                    free(filenames[j]);
                break;
            }

            fclose(file);
            free(filename);
        }
        free(filenames);

        //this will call close and update buffer.
        handleDisconnect(clientID);
    }
}

/**
 * Funkcija skirta patikrinti ar aplankas egzistuoja.
 *
 * @param path
 * @return
 */
bool DirectoryExists(const char* path) {
    DIR* dir = opendir(path);

    if(dir)
        return true;

    return false;
}

int main() {
    initializeClientBuffer();

    if (!DirectoryExists("data")) {
        if (mkdir("data", 0755) == 0) {
            write_error();
        }
    }

    SOCKET socket = createSocket();
    if (socket == INVALID_SOCKET)
        return -1;

    if(!configureSocketForReusingAddress(socket))
        write_named_error("Failed to configure socket!");

    while (true) {
        struct sockaddr_in address = requestPortFromConsoleInput();
        if (bindSocketToAddress(socket, &address))
            break;
    }

    makeSocketListenToConnections(socket, 5);

    while (true) {
        int maxFD = prepareFileDescriptorSetForSelect(socket);
        int res = select(maxFD + 1, &fileDescriptors, NULL, NULL, NULL);

        if (res < 0) {
            write_error();

            return -1;
        }

        if (FD_ISSET(socket, &fileDescriptors)) {
            makeSocketAcceptConnection(socket);
        }

        for (int i = 0; i < MAX_CLIENTS; i++) {
            if (clientBuffer[i].used) {
                if (FD_ISSET(clientBuffer[i].socket, &fileDescriptors)) {
                    char buffer[BUFFER_LENGTH];
                    memset(buffer, 0, sizeof(buffer));

                    int r_len = recv(clientBuffer[i].socket, buffer, BUFFER_LENGTH, 0);
                    if (r_len == -1 || r_len == 0)
                        handleDisconnect(i);

                    else {
                        handleClientMessage(i, buffer, socket);
                    }
                }
            }
        }
    }

    return 0;
}