#ifndef LOGGING_H
#define LOGGING_H

#include <stdio.h>
#include <stdarg.h>
#include <errno.h>

#include <netinet/in.h>
#include <arpa/inet.h>

/**
 * Funkcija skirta spausdinimui į ekraną.
 */
void write_log(const char *message, ...) {
    va_list args;
    va_start(args, message);

    vprintf(message, args);
    va_end(args);

    printf("\n");
}

/**
 * Writes an error
 */
void write_error() {
    write_log("ERROR: Code: %d", errno);
    write_log("ERROR: Message: %s", strerror(errno));
}

/**
 * Writes an error with title
 */
void write_named_error(const char* name) {
    write_log("ERROR: %s", name);
    write_error();
}

/**
 * Funkcija skirta atspausdinti adresa i konsole.
 *
 * @param address
 */
void printAddress(struct sockaddr_in const *address) {
    char *str = inet_ntoa(address->sin_addr);

    write_log("IP: %s", str);
    write_log("Port: %hu", address->sin_port);
}

#endif