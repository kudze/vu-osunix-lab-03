//
// Created by kudze on 2021-11-22.
//

#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <stdbool.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <errno.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <endian.h>
#include <math.h>

#include <netdb.h>

#include "logging.h"

bool resolveHostNameToIp(char * hostname , struct in_addr* host)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;

    if ( (he = gethostbyname( hostname ) ) == NULL) {
        write_error("Failed to resolve hostname");
        return false;
    }

    addr_list = (struct in_addr **) he->h_addr_list;

    for(i = 0; addr_list[i] != NULL; i++)
    {
        host->s_addr = addr_list[i]->s_addr;
        return true;
    }

    write_error("Failed to resolve hostname");
    return false;
}

bool hasSocketSubmittedAnyInput(SOCKET socket, time_t maxWaitSeconds) {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(socket, &fds);
    int maxFD = socket + 1;

    struct timeval tv = {maxWaitSeconds, 0};
    int res = select(maxFD, &fds, NULL, NULL, &tv);

    if (res < 0) {
        if (errno == EINTR)
            return false;

        write_named_error("select has failed for unknown reasons");
        return false;
    }

    if (res == 0) //0 means "may be zero if the timeout expired before any file descriptors became ready."
        return false;

    return FD_ISSET(socket, &fds);
}


/**
 * After executing will make file cursor to be at beggining.
 *
 * @param file
 * @return
 */
#define FILE_SIZE_TYPE unsigned long
#define FILE_SIZE_DOESNT_EXIST_VALUE 0L

FILE_SIZE_TYPE getFileSize(FILE *file) {
    if (file == NULL)
        return 0;

    fseek(file, 0L, SEEK_END);
    long size = ftell(file);
    fseek(file, 0L, SEEK_SET);

    if (size < 0)
        return FILE_SIZE_DOESNT_EXIST_VALUE;

    return (FILE_SIZE_TYPE) size;
}

bool checkIfSafePath(const char *filename) {
    if (strlen(filename) == 0)
        return true;

    if (filename[0] == '/' || filename[0] == '.')
        return false;

    return true;
}

char buffer[BUFFER_LENGTH];

void writeNoFileFoundIntoSocket(SOCKET socket) {
    FILE_SIZE_TYPE size_net_order = htobe64(FILE_SIZE_DOESNT_EXIST_VALUE);
    send(socket, &size_net_order, sizeof(FILE_SIZE_TYPE), 0);
}

//File must be open with rb.
//TODO: add verification of contents via hash check.
bool writeFileIntoSocket(SOCKET socket, FILE *file) {
    FILE_SIZE_TYPE size = getFileSize(file);
    write_log("File was found and weighs %ld bytes!", size);

    //Writing file size.
    FILE_SIZE_TYPE size_net_order = htobe64(size);
    send(socket, &size_net_order, sizeof(FILE_SIZE_TYPE), 0);

    //Writing file bytes.
    int readPackets = 0;
    for (size_t i = 0; i < size;) {
        size_t read = fread(buffer, sizeof(char), BUFFER_LENGTH, file);
        send(socket, buffer, read, 0);

        i += read;
        write_log("Sent %ld bytes via socket on %d packet! [TOTAL: %ld/%ld]", read, readPackets++, i, size);
    }

#ifndef IS_SERVER
    //Reading response from other side.
    memset(buffer, 0, sizeof(buffer));
    recv(socket, buffer, sizeof(buffer), 0);
    if (strcmp(buffer, "OK") == 0)
        return true;

    return false;
#else
    return true;
#endif
}

bool readFileFromSocket(SOCKET socket, FILE *file) {
    FILE_SIZE_TYPE size_net_order, size;

#ifdef IS_SERVER

    if(!hasSocketSubmittedAnyInput(socket, 1))
        return false;

#endif
    recv(socket, &size_net_order, sizeof(FILE_SIZE_TYPE), 0);
    size = be64toh(size_net_order);
    write_log("Reading (from socket %d) file of size %ld bytes", socket, size);

    if(size == FILE_SIZE_DOESNT_EXIST_VALUE)
        return false;

    FILE_SIZE_TYPE read = 0;
    unsigned int readPackets = 0;
    for (FILE_SIZE_TYPE readSize = 0; readSize < size; readSize += read) {
#ifdef IS_SERVER

        if(!hasSocketSubmittedAnyInput(socket, 1))
            return false;

#endif
        long bytesToRead = BUFFER_LENGTH;
        long sizeDiff = size - readSize;
        if(sizeDiff < BUFFER_LENGTH)
            bytesToRead = sizeDiff;

        read = recv(socket, buffer, bytesToRead, 0);
        if (read == 0) {
            write_log("Reading file from socket went unsuccessfully as other side has probably disconnected!");

            return false;
        }

        write_log("Read %ld bytes from %d nth packet! [TOTAL: %ld/%ld]", read, readPackets++, readSize + read, size);
        fwrite(buffer, sizeof(char), read, file);

        if (sizeDiff > BUFFER_LENGTH && read != BUFFER_LENGTH)
            return false;

        if (sizeDiff <= BUFFER_LENGTH && read != sizeDiff)
            return false;
    }

    return true;
}

void clearSocketBuffer(SOCKET socket) {
    while (true) {
        bool result = hasSocketSubmittedAnyInput(socket, 2);

        if (!result)
            break;

        if (recv(socket, buffer, BUFFER_LENGTH, 0) <= 0)
            break;
    }
}

#endif //ALGORITHMS_H
