//
// Created by kudze on 2021-11-22.
//

#ifndef INPUT_H
#define INPUT_H

#include <limits.h>
#include <arpa/inet.h>

#include "logging.h"
#include "algorithms.h"

/**
 * Funkcija skirta paprašyti vartotojo įvesti hostinimo adresą
 */
struct sockaddr_in requestPortFromConsoleInput() {
    struct sockaddr_in result = {};
    result.sin_family = AF_INET;
    result.sin_addr.s_addr = INADDR_ANY;

    while (true) {
        char buffer[20];

        write_log("Enter Port:");
        scanf("%s", buffer);
        char *end;
        long temp = strtol(buffer, &end, 10);

        if (temp < 0 || temp > USHRT_MAX || end == buffer || (*end) != '\0') {
            write_log("Incorrect port format!");
            continue;
        }

        result.sin_port = temp;
        return result;
    }
}

/**
 * Funkcija skirta paprašyti vartotojo įvesti prisijungimo adresą
 */
struct sockaddr_in requestAddressFromConsoleInput() {
    struct sockaddr_in result = {};
    result.sin_family = AF_INET;

    while (true) {
        char buffer[128];

        write_log("Enter IP address:");
        scanf("%s", buffer);

        resolveHostNameToIp(buffer, &result.sin_addr);
        if (strcmp(buffer, "localhost") == 0) {
            result.sin_addr.s_addr = inet_addr("127.0.0.1");
        } else {
            result.sin_addr.s_addr = inet_addr(buffer);
        }

        write_log("Enter Port:");
        scanf("%s", buffer);
        char *end;
        long temp = strtol(buffer, &end, 10);

        if (temp < 0 || temp > USHRT_MAX || end == buffer || (*end) != '\0') {
            write_log("Incorrect port format!");
            continue;
        }

        result.sin_port = temp;
        return result;
    }
}

#endif //INPUT_H
