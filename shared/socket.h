#ifndef SOCKET_H
#define SOCKET_H

#include <sys/socket.h>

#define SOCKET int
#define INVALID_SOCKET -1

/**
 * Funkcija skirta sukurti socket'ui (AF_INET = ipv4)
 *
 * @returns SOCKET - INVALID_SOCKET on failure
 */
SOCKET createSocket() {
    write_log("Creating socket...");

    SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (s == INVALID_SOCKET) {
        write_error("Failed to create a socket!");

        return s;
    }

    write_log("Socket %u was successfully created!", s);

    return s;
}

#endif