#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>
#include <netinet/in.h>

#include "../shared/shared.h"

fd_set fileDescriptors;

/**
 * Funkcija, kuri paruošia failu descriptorius select funkcijai.
 * Gražina maxfileDescriptor kuris naudojamas select'e.
 *
 * @param server_socket
 * @return
 */
int prepareFileDescriptorSetForSelect(SOCKET server_socket) {
    int maxfd = server_socket;

    FD_ZERO(&fileDescriptors);
    FD_SET(server_socket, &fileDescriptors);

    return maxfd + 1;
}

/**
 * Funkcija skirta prijungti socketą prie serverio.
 *
 * @param socket
 * @param address
 * @return bool - true on success.
 */
bool connectSocketToAddress(SOCKET socket, struct sockaddr_in const *address) {
    write_log("Connecting %u socket to:", socket);
    printAddress(address);

    int result = connect(socket, (const struct sockaddr *) address, sizeof(struct sockaddr_in));
    if (result == INVALID_SOCKET) {
        write_error();

        return false;
    }

    write_log("Successfully connected!");

    return true;
}

/**
 * Funkcija kuri papraso vartotojo ivesti string
 */
void requestStringFromConsoleInput(char *buffer, const char *message) {
    write_log(message);
    scanf("%s", buffer);
}

#define USER_ACTION_LIST 0
#define USER_ACTION_GET 1
#define USER_ACTION_UPLOAD 2
#define USER_ACTION_EXIT 3
#define USER_ACTION_CLONE_AND_EXIT 4

unsigned int requestActionFromCosnoleInput() {
    while (true) {
        write_log("");
        write_log("Available actions:");
        write_log("* list - Lists available files");
        write_log("* get - Downloads a file");
        write_log("* upload - Uploads a file");
        write_log("* exit - Terminates program");
        write_log("* clone - Fetches all files from server and terminates program");
        write_log("");
        write_log("Requested action:");

        char buffer[20];
        scanf("%s", buffer);
        if (strcmp(buffer, "list") == 0)
            return USER_ACTION_LIST;

        if (strcmp(buffer, "get") == 0)
            return USER_ACTION_GET;

        if (strcmp(buffer, "upload") == 0)
            return USER_ACTION_UPLOAD;

        if (strcmp(buffer, "exit") == 0)
            return USER_ACTION_EXIT;

        if (strcmp(buffer, "clone") == 0)
            return USER_ACTION_CLONE_AND_EXIT;

        write_log("Unknown action");
    }
}

int main() {
    SOCKET socket = createSocket();
    if (socket == INVALID_SOCKET)
        return -1;

    while (true) {
        struct sockaddr_in address = requestAddressFromConsoleInput();
        if (connectSocketToAddress(socket, &address))
            break;
    }

    while (true) {
        char buffer[1024];
        char filename[128];
        unsigned char action = requestActionFromCosnoleInput();

        if (action == USER_ACTION_LIST) {
            send(socket, "list", sizeof("list"), 0);

            memset(buffer, 0, sizeof(buffer));
            recv(socket, buffer, sizeof(buffer), 0);

            write_log("Server's files:");
            write_log("%s", buffer);
            continue;
        }

        if (action == USER_ACTION_GET) {
            memset(filename, 0, sizeof(filename));
            requestStringFromConsoleInput(filename, "Enter filename:");

            FILE *file = fopen(filename, "wb");
            if(file == NULL) {
                write_error("Could not create the file!");
                break;
            }

            memset(buffer, 0, sizeof(buffer));
            int written = snprintf(buffer, sizeof(buffer), "get|%s", filename);
            send(socket, buffer, written, 0);

            bool valid = readFileFromSocket(socket, file);
            if (!valid) {
                fclose(file);
                remove(filename);

                write_log("Downloading went unsuccessfully as some data was lost!");
                write_log("Clearing out socket buffer...");
                clearSocketBuffer(socket);
            }

            fclose(file);
            write_log("%s downloaded!", filename);
        }

        if (action == USER_ACTION_UPLOAD) {
            memset(filename, 0, sizeof(filename));
            requestStringFromConsoleInput(filename, "Enter filename:");

            if(!checkIfSafePath(filename))
            {
                write_error("Paths starting with . or / are disallowed!");
                continue;
            }

            FILE *file = fopen(filename, "rb");
            if (file == NULL) {
                write_error("File was not found when uploading to server!");
                continue;
            }

            memset(buffer, 0, sizeof(buffer));
            int written = snprintf(buffer, sizeof(buffer), "upload|%s", filename);
            send(socket, buffer, written, 0);

            bool result = writeFileIntoSocket(socket, file);
            fclose(file);

            if(result)
                write_log("File was uploaded successfully");
            else
                write_log("File was not uploaded successfully! :(");
        }

        if (action == USER_ACTION_EXIT) {
            write_log("Good evening!");
            break;
        }

        if (action == USER_ACTION_CLONE_AND_EXIT) {
            snprintf(buffer, sizeof(buffer), "clone");
            send(socket, buffer, 5, 0);

            shutdown(socket, SHUT_WR);
            unsigned long files_count_net_order, files_count;
            recv(socket, &files_count_net_order, sizeof(files_count_net_order), 0);
            files_count = be64toh(files_count_net_order);

            for(unsigned long i = 0; i < files_count; i++)
            {
                unsigned long filename_len_net_order, filename_len;
                recv(socket, &filename_len_net_order, sizeof(filename_len_net_order), 0);
                filename_len = be64toh(filename_len_net_order);

                char* filenameBuffer = calloc(filename_len + 1, sizeof(char));
                recv(socket, filenameBuffer, filename_len, 0);

                write_log("Cloning file %s with %ld characters!", filenameBuffer, filename_len);

                FILE *file = fopen(filenameBuffer, "wb");
                if(file == NULL) {
                    write_named_error("Could not create the file!");
                    clearSocketBuffer(socket);

                    free(filenameBuffer);
                    break;
                }

                bool valid = readFileFromSocket(socket, file);
                if(!valid) {
                    fclose(file);
                    remove(filenameBuffer);

                    write_log("Failed to read file %s from socket on clone command!", filenameBuffer);

                    free(filenameBuffer);
                    break;
                }

                fclose(file);
                free(filenameBuffer);
            }

            close(socket);
            write_log("Terminating...");
            exit(0);
        }
    }

    close(socket);

    return 0;
}